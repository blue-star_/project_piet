package com;

//import static com.db_types.*;

//import static com.db_types.*;

//import static com.Db_types.MONGO;

public class db_utils {


    private db_types db_types;
    private String dburl, username, query, password;

    public db_utils(db_types db_typess, String dburl, String username, String query, String password) {
        this.db_types = db_typess;
        this.dburl = dburl;
        this.username = username;
        this.query = query;
        this.password = password;
    }


    public void factory() {
        switch (db_types) {
            case MONGO:
                CRUD_MONGO crud_mongo = new CRUD_MONGO();
                crud_mongo.createConnection();
                crud_mongo.connectionStatus();
                crud_mongo.create();
                crud_mongo.read();
                crud_mongo.update();
                crud_mongo.delete();
                break;

            case RDBMS:
                CRUD_rdbms crud_rdbms = new CRUD_rdbms();
                crud_rdbms.createConnection();
                crud_rdbms.connectionStatus();
                crud_rdbms.create();
                crud_rdbms.read();
                crud_rdbms.update();
                crud_rdbms.delete();
                crud_rdbms.deleteConnection();
                break;

            case CASSANDRA:

                CRUD_CASSANDRA crud_cassandra = new CRUD_CASSANDRA();
                crud_cassandra.createConnection();
                crud_cassandra.connectionStatus();
                crud_cassandra.create();
                crud_cassandra.read();
                crud_cassandra.update();
                crud_cassandra.delete();
                crud_cassandra.deleteConnection();
                break;


        }
    }
}