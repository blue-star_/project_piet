package com;

public interface Icrud
{
    void create();
    void read();
    void update();
    void delete();

}
