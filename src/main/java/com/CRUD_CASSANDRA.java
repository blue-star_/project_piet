package com;

public class CRUD_CASSANDRA implements Icrud, Iconnection {
    @Override
    public void createConnection() {
        System.out.println("CASSANDRA connection successfully");
    }

    @Override
    public void connectionStatus() {
        System.out.println("check connection");

    }

    @Override
    public void deleteConnection() {
        System.out.println("remove connection");

    }

    @Override
    public void create() {
        System.out.println("create query");

    }

    @Override
    public void read() {
        System.out.println("read query");

    }

    @Override
    public void update() {
        System.out.println("update query");

    }

    @Override
    public void delete() {
        System.out.println("delete query");
    }
}
